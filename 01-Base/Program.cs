﻿using System;

namespace _01_Base
{
    // Une enumération est un ensemble de constante
    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    enum Direction : short { NORD = 90, EST = 0, SUD = 270, OUEST = 180 };

    [Flags]
    enum JourSemaine
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE
    }

    // Définition d'une structure
    struct Point
    {
        public int X;
        public int Y;
    }
    class Program
    {

        static void Main(string[] args)
        {
            #region Variable et type simple
            // Déclaration d'une variable   type nomVariable;
            int i;
            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable locale non initialisée

            // Initialisation    nomVariable = valeur
            i = 42;
            Console.WriteLine(i);

            // Déclaration multiple de variable 
            double hauteur, largeur;
            hauteur = 10.0;
            largeur = 5.0;
            Console.WriteLine(hauteur + " " + largeur); // + => concaténation

            // Déclaration et initialisation de variable    type nomVariable = valeur;
            int j = 12;
            Console.WriteLine(j);

            // Littéral caractère
            char chr = 's';
            char chrUtf8 = '\u0045';
            char chrHexUtf = '\x45';
            Console.WriteLine(chr + " " + chrUtf8 + " " + chrHexUtf);

            // Littéral booléen
            bool tst = false; // true
            Console.WriteLine(tst);

            // Littéral entier -> int par défaut
            long l1 = 12345678L;    // L->Littéral long
            ulong lu1 = 12345UL;    // U -> Litéral unsigned
            Console.WriteLine(l1 + " " + lu1);

            // Littéral nombre à virgule flottante -> double par défaut
            float f1 = 123.50F;     // F -> Littéral float
            decimal dm1 = 124.9M;   // M -> Littéral decimal
            Console.WriteLine(f1 + " " + dm1);

            // Littéral entier -> chagement de base
            int dec = 10;       // décimal (base 10) par défaut
            int hex = 0xff12;   // 0x => héxadécimal
            int bin = 0b101010; // 0b => binaire
            Console.WriteLine(dec + " " + hex + " " + bin);

            // Séparateur _
            double sep = 1_000_000.5;
            // double sd = _100_._00_;  // pas de séparateur en début, en fin , avant et après la virgule 
            Console.WriteLine(sep);

            // Littéral réel
            double d2 = 100.4;
            double dExp = 3.5e3;
            Console.WriteLine(d2 + " " + dExp);

            // Type implicite -> var
            var v1 = 10.5;   // v1 -> double
            var v2 = "nfvlnf";  // v2 -> string
            Console.WriteLine(v1 + " " + v2);

            //  avec @ on peut utiliser les mots réservés pour les nom de variable (à éviter) 
            int @var = 12;
            Console.WriteLine(@var);
            #endregion

            #region Conversion
            // Transtypage implicite ( pas de perte de donnée)
            // type inférieur => type supérieur
            int tii = 345;
            long lii = tii;
            double tid = tii;

            // Transtypage explicite: cast = (nouveauType) 
            double ted = 12.3;
            int tei = (int)ted;
            Console.WriteLine(ted + " " + tei);

            // Dépassement de capacité
            short sh1 = 300;        // 00000001 00101100    300
            sbyte sb1 = (sbyte)sh1; //          00101100    44
            Console.WriteLine(sh1 + "  " + sb1);

            // Checked / Unchecked
            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral
            //checked
            {
                sb1 = (sbyte)sh1; //44
                Console.WriteLine(sh1 + "  " + sb1);    // avec checked une exception est générée, s'il y a un dépassement de capacité
            }
            unchecked // (Par défaut)
            {
                sb1 = (sbyte)sh1;
                Console.WriteLine(sh1 + "  " + sb1);    // plus de vérification de dépassement de capacité
            }

            // Fonction de Convertion
            // La classe Convert contient des méthodes statiques permettant de convertir  un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérque
            int ii = 42;
            double cnv1 = Convert.ToDouble(ii); // Convertion d'un entier en double
            Console.WriteLine(cnv1);

            int cnv2 = Convert.ToInt32("123");  // Convertion d'une chaine de caractère  en entier
            Console.WriteLine(cnv2);

            // Conversion d'une chaine de caractères en entier
            Console.WriteLine(Int32.Parse("123"));
            // Console.WriteLine(Int32.Parse("123aaa"));        //  Erreur => génère une exception
            Console.WriteLine(Int32.TryParse("456", out ii));   // Retourne true et la convertion est affecté à ii
            Console.WriteLine(ii);
            Console.WriteLine(Int32.TryParse("789fsii", out ii));   // Erreur => retourne false , 0 est affecté à ii
            Console.WriteLine(ii);
            #endregion

            #region Type référence
            string str1 = "Hello"; // new string("Hello");
            string str2 = null;
            Console.WriteLine(str1 + " " + str2);
            str2 = str1;
            Console.WriteLine(str1 + " " + str2);
            str1 = null;
            str2 = null;    //str1 et str2 sont égales à null
                            // Il n'y a plus de référence sur l'objet, il éligible à la destruction par le garbage collector
            #endregion

            // Saisie d'un valeur au clavier
            string ligne = Console.ReadLine();
            Console.WriteLine(ligne);
            #region Exercices
            // Exercice: Salutation
            // Faire un programme qui:
            // - Affiche le message: Entrer votre nom
            // - Permet de saisir le nom
            // - Affiche Bonjour, complété du nom saisie
            Console.WriteLine("Entrer votre nom");
            string nom = Console.ReadLine();
            Console.WriteLine("Bonjour, " + nom);

            // Exercice: Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 = 4
            string val = Console.ReadLine();
            int va = Convert.ToInt32(val);
            int vb = Convert.ToInt32(Console.ReadLine());
            int res = va + vb;
            Console.WriteLine(va + " + " + vb + " = " + res);
            #endregion

            // Nullable : permet d'utiliser une valeur null avec un type valeur
            double? n = null;
            Console.WriteLine(n.HasValue);   // La propriété HasValue retourne true si n contient une valeur (!= null) 
            n = 23.7;
            Console.WriteLine(n.HasValue);
            double ddd = 10.55;
            n = ddd; // implicite

            double d4 = (double)n; // Pour récupérer la valeur on peur faireun cast
            double d5 = n.Value;    // ou  on peut utiliser la propriété Value
            Console.Write(d4 + " " + d5);

            // Constante
            const double PI = 3.14;
            // PI = 3.1419  // Erreur: on ne peut pas modifier la valeur d'une constante
            Console.WriteLine(2 * PI);

            #region Opérateur
            // Opérateur arithméthique
            int op1 = 56;
            int op2 = 4;
            int res2 = op1 + op2;
            Console.WriteLine(res2);
            res2 = op1 % 4;      // % => modulo (reste de division entière)
            Console.WriteLine(res2);

            // Opérateur d'incrémentation
            // Pré-incrémentation
            int inc = 0;
            res2 = ++inc; // inc=1 res2=1
            Console.WriteLine(inc + " " + res2);

            // Post-incrémentation
            inc = 0;
            res2 = inc++; // inc=1 res2=0
            Console.WriteLine(inc + " " + res2);

            // Affectation composée
            double ac = 12.6;
            ac += 40.5; //ac=ac+40.5

            // Opérateur de comparaison
            bool test1 = ac < 10.0; // Une comparaison a pour résultat un booléen
            Console.WriteLine(test1);
            test1 = inc == 1;
            Console.WriteLine(test1);

            // Opérateur logique
            test1 = !(inc == 1);
            Console.WriteLine(test1);
            // Opérateur court-circuit && et ||
            // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            test1 = ac < 10.0 && inc == 1;   // comme ac < 10.0 est faux,  inc == 1 n'est pas évalué
            Console.WriteLine(test1);
            // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            test1 = inc == 1 || ac > 10.0;
            Console.WriteLine(test1);

            // Opérateur Binaires (bit à bit)
            byte b = 0b11010;
            Console.WriteLine(~b);          // Opérateur ~ => complémént: 1-> 0 et 0-> 1
            Console.WriteLine(b & 0b110);   //          et => 010   2
            Console.WriteLine(b | 0b110);   //          ou => 11110 30
            Console.WriteLine(b ^ 0b110);   // ou exclusif => 11100 28
            Console.WriteLine(b >> 1);      // Décalage à droite de 1   1101
            Console.WriteLine(b << 2);      // Décalage à gauche de 2   1101000

            //  L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null ;
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str21 = null;
            string str22 = str21 ?? "Default";
            Console.WriteLine(str22);       // Default
            str21 = "Hello";
            str22 = str21 ?? "Default";
            Console.WriteLine(str22);       // Hello
            #endregion

            #region Promotion numérique
            double prnd = 123.67;
            int prni = 11;
            double prnres = prnd + prni;    // prni est promu en double => Le type le + petit est promu vers le + grand type des deux
            Console.WriteLine(prnres);
            Console.WriteLine(prni / 2);
            Console.WriteLine(prni / 2.0);  // prni est promu en double

            sbyte b1 = 1;
            sbyte b2 = 2;       // sbyte, byte, short, ushort, char sont promus en int
            int b3 = b1 + b2;   // b1 et b2 sont promus en int
            Console.WriteLine(b3);
            #endregion

            // Format de chaine de caractères
            int xi = 34;
            int yi = 2;
            string resFormat = string.Format("xi={0} yi={1}", xi, yi);
            Console.WriteLine(resFormat);

            Console.WriteLine("xi={0} yi={1}", xi, yi); // on peut définir directement le format dans la mèthode WriteLine 

            Console.WriteLine($"xi={xi}, yi={yi}");

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            // Chaînes textuelles => @ devant une littérale chaine de caractères
            // Conserve les retours à la ligne, n'interpréte pas les caractères spéciaux (utile pour les chemins de fichiers) 
            Console.WriteLine(@"c:\tmp\newfile.txt");

            // Conversion un nombre sous forme de chaine dans une autre base
            Console.WriteLine(Convert.ToString(23, 2)); // affichage en binaire
            Console.WriteLine(Convert.ToString(23, 16)); // affichage en héxadécimal


            #region Enumération
            // dir est une variable qui ne pourra accepter que les valeurs de l'enum Direction
            Direction dir = Direction.SUD;

            // enum ->  entier (cast)
            int ditInt = (int)dir;
            Console.WriteLine(ditInt);

            // entier -> enum (cast)
            dir = (Direction)180;   // OUEST

            // La méthode toString convertit une constante enummérée en une chaine de caractère
            Console.WriteLine(dir.ToString());

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            dir = (Direction)Enum.Parse(typeof(Direction), "SUD");
            Console.WriteLine(dir.ToString());
            // dir = (Direction)Enum.Parse(typeof(Direction), "SUD2"); // si la chaine n'existe pas dans l'énumation => exception
            //Console.WriteLine(dir.ToString());

            // Énumération comme indicateurs binaires
            JourSemaine jours = JourSemaine.LUNDI | JourSemaine.JEUDI;

            Console.WriteLine((jours & JourSemaine.JEUDI) != 0);     // teste la présence de JEUDI => true
            Console.WriteLine((jours & JourSemaine.MERCREDI) != 0);   // teste la présence de MERCREDI => false
            Console.WriteLine((jours & JourSemaine.WEEKEND) != 0);

            jours |= JourSemaine.DIMANCHE;
            Console.WriteLine((jours & JourSemaine.WEEKEND) != 0);
            #endregion

            // Structure
            Point p1;
            p1.X = 1;   // accès au champs X de la structure
            p1.Y = 6;
            Console.WriteLine($"x={p1.X} y={p1.Y}");

            Console.ReadKey();
        }
    }
}
