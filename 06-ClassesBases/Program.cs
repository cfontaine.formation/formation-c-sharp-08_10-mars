﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace _07_ClassesBases
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Chaine de caractère
            string str = "Hello";
            string str2 = new string('a', 10);
            Console.WriteLine(str);
            Console.WriteLine(str2);

            // Length -> Nombre de caractère de la chaine de caractère
            Console.WriteLine(str2.Length);

            // On peut accèder à un caractère de la chaine comme à un élément d'un tableau
            Console.WriteLine(str[1]);  // e

            // Concaténation 
            // avec l'opérateur + ou avec la méthode de classe Concat
            string str3 = string.Concat(str, " World");
            Console.WriteLine(str3);

            // La méthode Join concatènent les chaines, en les séparants par un caractère (ou une chaine) de séparation
            string csv = string.Join(";", str, "Azertty", "John");
            Console.WriteLine(csv);

            // Découpe la chaine en un tableau de sous-chaine suivant un ou plusieurs séparateur, par défaut le caractère espace
            string calcul = "10 + 4";
            string[] tabStr = calcul.Split(' ');
            foreach (var s in tabStr)
            {
                Console.WriteLine(s);
            }

            // Substring permet d'extraire une sous-chaine d'une chaine
            // de l'indice passé en paramètre juqu'à la fin de la chaine
            Console.WriteLine(str3.Substring(6));
            // ou pour le nombre de caractères passé en paramètre
            Console.WriteLine(str3.Substring(4, 3));

            // Insère la chaine à partir de l'indice passé en paramètre
            Console.WriteLine(str3.Insert(5, "-------"));

            // Les caractères sont supprimés à partir de l'indice pour le nombre de caractère passé en paramètre
            Console.WriteLine(str3.Remove(5, 1));

            // StartsWith retourne true si la chaine commence par la chaine passé en paramètre
            Console.WriteLine(str3.StartsWith("Hell"));
            Console.WriteLine(str3.StartsWith("aaaa"));

            // IndexOf retourne la première occurence du caractère ou de la chaine passée en paramètre
            Console.WriteLine(str3.IndexOf('o'));
            Console.WriteLine(str3.IndexOf('o', 5));     // idem mais à partir de l'indice passé en paramètre
            Console.WriteLine(str3.IndexOf('o', 8));    // retourne -1, si le caractère n'est pas trouvé 

            // Retourne True si la sous-chaine passée en paramètre est contenu dans la chaine
            Console.WriteLine(str3.Contains("Wo"));
            Console.WriteLine(str3.Contains("1234"));

            // Remplace toutes les chaines (ou caratère) oldValue par newValue 
            Console.WriteLine(str3.Replace('o', 'a'));

            // Aligne les caractères à droite en les complétant par un caractère à gauche pour une longueur spécifiée
            Console.WriteLine(str3.PadLeft(40, '_'));

            // Aligne les caractères à gauche en les complétant par un caractère à droite pour une longueur spécifiée
            Console.WriteLine(str3.PadRight(40, '_'));

            // Trim supprime les caractères de blanc du début et de la fin de la chaine
            string strSaisie = "   \n \t   azerrty  qsdgfhgh \t \t \n\n  ";
            Console.WriteLine(strSaisie.Trim());
            Console.WriteLine(strSaisie.TrimStart());       // idem uniquement en début de chaine
            Console.WriteLine(strSaisie.TrimEnd());         // idem uniquement en fin de chaine

            // ToUpper convertie tous les caractères en majuscule
            Console.WriteLine(str3.ToUpper());

            // ToLower convertie tous les caractères en minuscule
            Console.WriteLine(str3.ToLower());

            // On peut chainer l'appel des différentes méthodes
            Console.WriteLine(str3.ToUpper().TrimEnd().Substring(5));

            // Comparaison 0-> égale, 1-> se trouve après, -1 -> se trouve avant dans l'ordre alphabétique
            // Il existe 2 méthodes: une d'instance et de classe
            Console.WriteLine(str.CompareTo("Bonjour"));
            Console.WriteLine(string.Compare("Bonjour", str));

            // On peut tester l'égalité de 2 chaines de caractères avec Equals et l'opérateur ==
            Console.WriteLine(string.Equals("Hello", str));
            Console.WriteLine("Hello" == str);

            // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation et insertion, remplacement,supression de sous-chaine)
            // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un objet String => pas de création d'objet intermédiare
            StringBuilder sb = new StringBuilder("test");
            sb.Append(23);
            sb.Append("HELLO");
            Console.WriteLine(sb);
            string str4 = sb.ToString(); // Convertion d'un StringBuilder en une chaine de caractères 
            Console.WriteLine(str4);

            // Exercice Inversion de chaine
            Console.WriteLine(Inverser("Bonjour"));

            // Exercice Palindrome
            Console.WriteLine(Palindrome("Bonjour"));
            Console.WriteLine(Palindrome("Radar   "));

            // Exercice Acronyme
            Console.WriteLine(Acronyme("Organisation du traité de l’Atlantique Nord"));
            Console.WriteLine(Acronyme("Développement d’application web en aglomération nantaise"));
            // Exercice Calculatrice
            try
            {
                Console.WriteLine(Calculatrice(Console.ReadLine(), out _));
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArithmeticException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            #endregion

            #region Date
            DateTime d = DateTime.Now;    // DateTime.Now => récupérer l'heure et la date courante
            Console.WriteLine(d);

            Console.WriteLine(d.Month);
            Console.WriteLine(d.Day);
            Console.WriteLine(d.Hour);

            DateTime noel2019 = new DateTime(2019, 12, 25);
            Console.WriteLine(noel2019);
            Console.WriteLine(d - noel2019);

            TimeSpan dixjours = new TimeSpan(10, 0, 0, 0);  // TimeSpan => représente une durée
            Console.WriteLine(d.Add(dixjours));
            Console.WriteLine(d.AddDays(4));
            Console.WriteLine(d.ToLongDateString());
            Console.WriteLine(d.ToShortDateString());
            Console.WriteLine(d.ToLongTimeString());
            Console.WriteLine(d.ToShortTimeString());
            Console.WriteLine(d.ToString("yy-MMM-dd"));
            Console.WriteLine(DateTime.Parse("2003/07/12"));
            #endregion

            #region Collection
            // Collection faiblement typé => elle peut contenir tous types d'objets
            ArrayList lst = new ArrayList();
            lst.Add("azerty");
            lst.Add(123);
            Console.WriteLine(lst.Count);   // Nombre d'élément de la collection
            Console.WriteLine(lst[0]);
            string va = (string)lst[0];

            // Collection fortement typée => type générique
            List<int> lstF = new List<int>();
            lstF.Add(3);
            lstF.Add(7);
            lstF.Add(33);
            lstF.Add(17);
            //lstF.Add("azerty");           // On ne peut plus qu'ajouter des entiers => sinon erreur de complilation
            Console.WriteLine(lstF[1]);     // Accès à un élément de la liste
            foreach (var v in lstF)         // Parcourir la collection complétement
            {
                Console.WriteLine(v);
            }

            lstF.Reverse();     // Inverser l'ordre de la liste 
            foreach (var v in lstF)
            {
                Console.WriteLine(v);
            }

            // Dictionary => association clé/valeur
            Dictionary<int, string> m = new Dictionary<int, string>();
            m.Add(4, "Bonjour");    // Add => ajout d'un valeur associé à une clé
            // m.Add(4, "Other");   // On ne peut pas ajouter si la clé éxiste déjà
            m.Add(61, "World");
            m.Add(321, "Hello");
            m.Add(23, "John Doe");
            Console.WriteLine(m[321]);   // accès à un élément m[clé] => valeur
            m[61] = "Monde";
            m.Remove(23);
            m[3] = "test";
            Console.WriteLine(m[3]);

            // Parcourir un dictionnary
            foreach (var kv in m)   // KeyValuePair<int,string>
            {
                Console.WriteLine($"Key={kv.Key} Value={kv.Value}");
            }

            // Parcourrir un collection avec un Enumérateur
            IEnumerator it = lstF.GetEnumerator();
            it.Reset();
            while (it.MoveNext())
            {
                Console.WriteLine(it.Current);
            }
            Console.ReadKey();
        }
        #endregion

        // Exercice Inversion de chaine
        // Écrire la fonction Inversee qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
        // bonjour →  ruojnob
        static string Inverser(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = str.Length - 1; i >= 0; i--)
            {
                sb.Append(str[i]);
            }
            return sb.ToString();
        }

        // Exercice  Palindrome
        // Écrire une méthode __Palindrome__ qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
        //    SOS →  true
        //    Bonjour →  false
        //    radar →  true
        static bool Palindrome(string str)
        {
            string tmp = str.ToLower().Trim();
            return tmp == Inverser(tmp);
        }

        //Exercice Faire une méthode de classe qui prend en paramètre une phrase et qui retourne un acronyme
        //Les mots de la phrase sont pris en compte s'ils contiennent plus de 2 lettres
        //Ex:   Comité international olympique → CIO
        //      Organisation du traité de l’Atlantique Nord → OTAN
        static string Acronyme(string str)
        {
            string acr = "";
            string[] tabStr = str.Trim().ToUpper().Replace('’', ' ').Split();
            foreach (string s in tabStr)
            {
                if (s.Length > 2)
                {
                    acr += s[0];
                }
            }
            return acr;
        }

        static string Calculatrice(string operation, out double result)
        {
            string op = SplitOperation(operation, out double v1, out double v2);
            switch (op)
            {
                case "+":
                    result = v1 + v2;
                    break;
                case "-":
                    result = v1 - v2;
                    break;
                case "*":
                    result = v1 * v2;
                    break;
                case "/":
                    if (v2 == 0.0)
                    {
                        throw new ArithmeticException("Division par 0");
                    }
                    else
                    {
                        result = v1 / v2;
                    }
                    break;
                default:
                    throw new Exception($"L'opérateur {op} est incorrecte");
            }
            return $"{v1} {op} {v2} = {result}";
        }
        static string SplitOperation(string operation, out double v1, out double v2)
        {
            string[] elm = operation.Split();
            if (elm.Length != 3)
            {
                throw new FormatException(elm.Length < 3 ? "Il manque des éléments dans l'opération " : "Il y a trop d'élément dans l'opération ");
            }
            v1 = Convert.ToDouble(elm[0]);
            v2 = Convert.ToDouble(elm[2]);
            return elm[1];
        }
    }


}
