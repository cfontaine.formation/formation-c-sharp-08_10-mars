﻿using System;
using System.IO;

namespace _05_Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            int val;
            int[] t = new int[4];
            try
            {
                val = Convert.ToInt32(Console.ReadLine());  // Génére une Exception FormatException
                t[10] = val;                                // Génére une Exception IndexOutOfRangeException 
                File.OpenRead("nexistepas");                // Génère  une Exception FileNotFoundException
                Console.WriteLine("Lecture des données");
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine($"Le fichier n'existe pas ({e.Message})");     // Message => permet de récupérer le messsage de l'exception
            }
            catch (FormatException e)
            {
                Console.WriteLine("Mauvais format");
                Console.WriteLine(e.StackTrace);
            }
            catch (Exception e)                 // Attrape tous les autres exception
            {
                Console.WriteLine($"Une erreur c'est produite {e.Message}");
            }
            finally   // Le bloc finally est toujours éxécuté
            {
                Console.WriteLine("liberation de ressource");
            }

            try
            {
                int age;
                age = Convert.ToInt32(Console.ReadLine());
                TraitementEmploye(age);
            }
            catch (Exception e) when (e.Message == "Erreur Traitement employe")
            {
                Console.WriteLine("Main " + e.Message);
                Console.WriteLine("Main " + e.StackTrace);
            }
            catch (Exception e) when (e.Message == "age négatif")
            {
                Console.WriteLine("Main " + e.Message);
                Console.WriteLine("Main " + e.StackTrace);
            }
            Console.WriteLine("Suite du programme");



            try
            {
                double v1 = Convert.ToDouble(Console.ReadLine());
                string op = Console.ReadLine();
                double v2 = Convert.ToDouble(Console.ReadLine());
                Calculatrice(v1, op, v2);
            }
            catch (ArithmeticException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (FormatException e)
            {
                Console.WriteLine("Erreur lors de la saisie");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


            Console.ReadKey();
        }

        static void TraitementEmploye(int age)
        {
            Console.WriteLine("Traitement  Employe... ");
            try
            {
                TraitementAge(age);
            }
            catch (Exception e)
            {
                Console.WriteLine("TraitementEmploye " + e.Message);
                throw new Exception("Erreur Traitement employe", e);
            }
            Console.WriteLine("Suite Traitement Employe...");
        }
        static void TraitementAge(int age)
        {
            Console.WriteLine("Traitement ...");
            if (age < 0)
            {
                throw new Exception("age négatif");
            }
            Console.WriteLine("Suite Traitement ...");
        }

        static void Calculatrice(double v1, string op, double v2)
        {
            switch (op)
            {
                case "+":
                    Console.WriteLine($"{v1} + {v2} = {v1 + v2}");
                    break;
                case "-":
                    Console.WriteLine($"{v1} - {v2} = {v1 - v2}");
                    break;
                case "*":
                    Console.WriteLine($" {v1} * {v2} = {v1 * v2}");
                    break;
                case "/":
                    if (v2 == 0.0)
                    {
                        throw new ArithmeticException("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine($" {v1} / {v2} = { v1 / v2}");
                    }
                    break;
                default:
                    throw new Exception($"L'opérateur {op} est incorrecte");
            }

        }
    }
}
