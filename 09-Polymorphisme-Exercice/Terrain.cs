﻿namespace _09_Polymorphisme_Exercice
{
    class Terrain
    {
        Forme[] TabForme = new Forme[10];
        int nbForme;

        public Terrain()
        {
        }

        public void Add(Forme forme)
        {
            if (nbForme < TabForme.Length)
            {
                TabForme[nbForme] = forme;
                nbForme++;
            }
        }

        public double SurfaceTotal()
        {
            double surface = 0.0;
            for (int i = 0; i < nbForme; i++)
            {
                surface += TabForme[i].CalculSurface();
            }
            return surface;
        }

        public double SurfaceCouleur(Couleurs couleur)
        {
            double surface = 0.0;
            for (int i = 0; i < nbForme; i++)
            {
                if (TabForme[i].Couleur == couleur)
                {
                    surface += TabForme[i].CalculSurface();
                }
            }
            return surface;
        }
    }

}
