﻿namespace _09_Polymorphisme_Exercice
{
    class TriangleRectangle : Rectangle
    {
        public TriangleRectangle(double largeur, double hauteur, Couleurs couleur) : base(largeur, hauteur, couleur)
        {

        }

        public override double CalculSurface()
        {
            return base.CalculSurface() / 2.0;
        }

        public override string ToString()
        {
            return string.Format($"TriangleRectangle [ {base.ToString()}]");
        }
    }

}
