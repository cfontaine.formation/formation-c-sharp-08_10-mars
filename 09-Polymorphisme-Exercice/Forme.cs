﻿namespace _09_Polymorphisme_Exercice
{
    enum Couleurs { VERT, BLEU, ROUGE, ORANGE };

    abstract class Forme
    {
        public Couleurs Couleur { get; set; }

        public Forme(Couleurs couleur)
        {
            Couleur = couleur;
        }

        public abstract double CalculSurface();

        public override string ToString()
        {
            return string.Format("Forme[ {0} ]", Couleur.ToString());
        }
    }

}
