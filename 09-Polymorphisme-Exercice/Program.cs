﻿using System;

namespace _09_Polymorphisme_Exercice
{
    // Classe Forme 
    // Attribut
    // - une couleur(une couleur peut être vert, rouge, bleu, orange)

    // Méthodes
    // - Une méthode qui retourne la couleur de la forme
    // - Une méthode qui permet de calculer la surface de la forme

    // Classe Cercle qui hérite de Forme
    // Attribut
    // - un rayon

    // Classe Rectangle qui hérite de Forme
    // - une largeur
    // - une longueur

    // Classe Triangle rectangle
    // - une largeur
    // - une longueur

    // Classe Terrain
    // Attribut
    // - un tableau de forme, un terrain contient au maximum 10 formes
    // - le nombre de forme que contient le terrain

    // Méthodes
    // - Une méthode pour ajouter une forme au terrain
    // - Une méthode qui retoune la surface total du terrain
    // - Une méthode qui retoune la surface du terrain en fonction d'une couleur

    class Program
    {
        static void Main(string[] args)
        {
            Terrain t = new Terrain();
            t.Add(new TriangleRectangle(2.0, 2.0, Couleurs.VERT));
            t.Add(new Rectangle(2.0, 2.0, Couleurs.BLEU));
            t.Add(new Cercle(Couleurs.ORANGE, 1.0));
            t.Add(new Rectangle(2.0, 2.0, Couleurs.BLEU));
            t.Add(new Cercle(Couleurs.ORANGE, 1.0));
            t.Add(new Rectangle(2.0, 2.0, Couleurs.BLEU));
            t.Add(new TriangleRectangle(2.0, 2.0, Couleurs.VERT));
            t.Add(new Rectangle(1.0, 1.0, Couleurs.ROUGE));

            Console.WriteLine("Surface total= {0}", t.SurfaceTotal());
            Console.WriteLine("Surface Orange= {0}", t.SurfaceCouleur(Couleurs.ORANGE));
            Console.WriteLine("Surface Vert= {0}", t.SurfaceCouleur(Couleurs.VERT));
            Console.WriteLine("Surface Bleu= {0}", t.SurfaceCouleur(Couleurs.BLEU));
            Console.WriteLine("Surface Rouge= {0}", t.SurfaceCouleur(Couleurs.ROUGE));
            Console.ReadKey();
        }
    }

}
