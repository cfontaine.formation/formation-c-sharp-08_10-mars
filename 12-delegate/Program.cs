﻿using System;
using System.Text.RegularExpressions;

namespace _12_delegate
{
    class Program
    {
        // Déléguation => Définition de prototypes de fonctions
        public delegate int Operation(int n1, int n2);
        public delegate bool Comparaison(int n1, int n2);

        // C# 1
        // Méthodes correspondants au prototype Operation
        // Retourne un entier et a pour paramètre 2 entiers
        public static int Ajouter(int n1, int n2)
        {
            return n1 + n2;
        }

        public static int Multiplier(int a, int b)
        {
            return a * b;
        }

        static void Main(string[] args)
        {
            // Appel d'un délégué C# 1
            Operation op = new Operation(Ajouter);
            AfficherCalcul(1, 2, op);
            AfficherCalcul(3, 4, new Operation(Multiplier));

            // Appel d'un délégué C# 2.0 => plus besoin de méthode correspondant au prototype
            Operation op2 = delegate (int a, int b) { return a - b; };
            AfficherCalcul(1, 2, op2);
            AfficherCalcul(3, 4, delegate (int a, int b) { return a * b; });

            // Appel d'un délégué C# 3.0 => utilisation des lambdas
            // Lambda =>méthode sans nom, les paramètres et le corps de la méthode sont séparés par l'opérateur =>

            // Syntaxe
            // () => expression     S'il n'y a pas de paramètre
            // 1 paramètre => expression
            // (paramètres) => expression
            // (paramètres) => { instructions }
            AfficherCalcul(3, 4, (n1, n2) => n1 * n2);
            AfficherCalcul(3, 4, (n1, n2) => n1 + n2);

            // Excercice delegate
            int[] tab = { 1, 4, 3, -6, 10, 5, -8, 1, -4 };
            SortTab(tab, (a, b) => a > b);
            AfficherTableau(tab);
            SortTab(tab, (a, b) => a < b);
            AfficherTableau(tab);

            //Expression réguliaire
            Regex reg = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            string str = Console.ReadLine();
            if (reg.IsMatch(str))
            {
                Console.WriteLine("Email Valide");
            }

            Console.ReadKey();
        }

        // On utilise les délégués pour passer une méthode en paramètre à une autre méthode
        public static void AfficherCalcul(int a, int b, Operation op)
        {
            Console.WriteLine(op(a, b));
        }

        // Excercice delegate
        //- Ecrire le delegate Comparaison pour avoir un tri croissant ou décroissant
        //- Ecrire un lamba pour tri décroissant
        static void SortTab(int[] tab, Comparaison cmp)
        {
            bool notDone = true;
            int end = tab.Length - 1;
            while (notDone)
            {
                notDone = false;
                for (int i = 0; i < end; i++)
                {
                    if (cmp(tab[i], tab[i + 1]))
                    {
                        int tmp = tab[i];
                        tab[i] = tab[i + 1];
                        tab[i + 1] = tmp;
                        notDone = true;
                    }
                }
                end--;
            }
        }

        static void AfficherTableau(int[] tab)
        {
            Console.Write("[");
            foreach (int t in tab)
            {
                Console.Write($" {t}");
            }
            Console.WriteLine(" ]");
        }

    }
}
