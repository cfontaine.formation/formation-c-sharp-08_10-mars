﻿using System;
using System.Collections.Generic;
using System.IO;

namespace _14_LibContactBdd
{
    public static class ExportContactCsv
    {
        public static List<Contact> Import(string path)
        {
            List<Contact> contacts = new List<Contact>();
            using (StreamReader reader = new StreamReader(path))
            {
                for (; ; )
                {
                    string line = reader.ReadLine();
                    if (line == null)
                    {
                        return contacts;
                    }
                    string[] tokens = line.Split(';');
                    if (tokens.Length == 4)
                    {
                        try
                        {
                            contacts.Add(new Contact(tokens[0], tokens[1], DateTime.Parse(tokens[2]), tokens[3]));
                        }
                        catch (FormatException e)
                        {
                            throw new ContactExportException("Mauvais format de date", e);
                        }
                    }
                    else
                    {
                        throw new ContactExportException("Une ligne ne contient pas 4 éléments");
                    }
                }
            }
        }

        public static void Export(List<Contact> contacts, string path)
        {
            using (StreamWriter writer = new StreamWriter(path))
            {
                foreach (Contact c in contacts)
                {
                    writer.WriteLine(string.Format($"{c.Prenom};{c.Nom};{c.JourNaissance.ToString()};{c.Email}"));
                }
            }
        }
    }
}
