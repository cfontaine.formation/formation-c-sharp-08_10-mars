﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace _14_LibContactBdd
{
    public class ContactDao : GenericDao<Contact>
    {
        protected override void Create(MySqlConnection cnx, Contact elm)
        {
            string req = "INSERT INTO contacts(prenom,nom,jour_naissance,email)VALUES (@prenom,@nom,@jourNaissance,@email); ";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@prenom", elm.Prenom);
            cmd.Parameters.AddWithValue("@nom", elm.Nom);
            cmd.Parameters.AddWithValue("@jourNaissance", elm.JourNaissance);
            cmd.Parameters.AddWithValue("@email", elm.Email);
            cmd.ExecuteNonQuery();
            elm.Id = cmd.LastInsertedId;
        }

        protected override void Delete(MySqlConnection cnx, Contact elm)
        {
            string req = "DELETE FROM contacts WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@id", elm.Id);
            cmd.ExecuteNonQuery();
        }

        protected override void Update(MySqlConnection cnx, Contact elm)
        {
            string req = "UPDATE contacts SET nom = @nom, prenom=@prenom, jour_naissance=@jourNaissance, email=@email  WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@id", elm.Id);
            cmd.Parameters.AddWithValue("@prenom", elm.Prenom);
            cmd.Parameters.AddWithValue("@nom", elm.Nom);
            cmd.Parameters.AddWithValue("@jourNaissance", elm.JourNaissance);
            cmd.Parameters.AddWithValue("@email", elm.Email);
            cmd.ExecuteNonQuery();
        }

        protected override Contact Read(MySqlConnection cnx, long id)
        {
            Contact contact = null;
            string req = "SELECT id,prenom,nom,jour_naissance,email FROM contacts  WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                contact = new Contact(reader.GetString("prenom"), reader.GetString("nom"), reader.GetDateTime("jour_naissance"), reader.GetString("email"));
                contact.Id = reader.GetInt64("id");
            }
            return contact;
        }

        protected override List<Contact> ReadAll(MySqlConnection cnx)
        {
            List<Contact> lst = new List<Contact>();
            string req = "SELECT id,prenom,nom,jour_naissance,email FROM contacts";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Contact c = new Contact(reader.GetString("prenom"), reader.GetString("nom"), reader.GetDateTime("jour_naissance"), reader.GetString("email"));
                c.Id = reader.GetInt64("id");
                lst.Add(c);
            }
            return lst;
        }

        // Dans le dao, on peut déclarer d'autre méthodes suplémentaires uniquement pour la classe Contact 
        public bool IsEmailExist(string email, bool close = true)   // test si un email existe dans la base de donnée
        {
            bool exist = false;
            MySqlConnection cnx = GetConnection();
            string req = "SELECT email FROM contacts WHERE email=@email";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@email", email);
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                exist = true;
            }
            CloseConnection(close);
            return exist;
        }

        public Contact FindByEmail(string email, bool close = true)    // Trouve un contact dans la bdd en fonction d'un email 
        {
            Contact c = null;
            MySqlConnection cnx = GetConnection();
            string req = "SELECT id,prenom,nom,jour_naissance,email FROM contacts WHERE email=@email";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@email", email);
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                c = new Contact(reader.GetString("prenom"), reader.GetString("nom"), reader.GetDateTime("jour_naissance"), email);
                c.Id = reader.GetInt64("id");
            }
            CloseConnection(close);
            return c;
        }
    }

}
