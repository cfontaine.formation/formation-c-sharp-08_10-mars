﻿using System;

namespace _14_LibContactBdd
{
    public class Contact : DbObject
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public DateTime JourNaissance { get; set; }

        public string Email { get; set; }

        public Contact(string prenom, string nom, DateTime jourNaissance, string email)
        {
            Prenom = prenom;
            Nom = nom;
            JourNaissance = jourNaissance;
            Email = email;
        }

        public override string ToString()
        {
            return string.Format($"[ Id: {base.ToString()}] {Prenom} {Nom}\t{JourNaissance}\t{Email}]");
        }
    }

}
