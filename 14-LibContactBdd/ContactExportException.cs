﻿using System;

namespace _14_LibContactBdd
{
    public class ContactExportException : Exception
    {
        public ContactExportException() : base()
        {

        }

        public ContactExportException(string message) : base(message)
        {
        }

        public ContactExportException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
