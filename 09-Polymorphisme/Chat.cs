﻿using System;

namespace _09_Polymorphisme
{
    class Chat : Animal, IPeutMarcher
    {
        public int NbVie { get; set; } = 9;

        public Chat(int nbVie, int age, double poid) : base(age, poid)
        {
            NbVie = nbVie;
        }

        public override void EmettreUnSon()
        {
            Console.WriteLine("Le Chat miaule");
        }

        public void Marcher()
        {
            Console.WriteLine("Le chat marche");
        }

        public void Courrir()
        {
            Console.WriteLine("Le chat court");
        }
    }
}
