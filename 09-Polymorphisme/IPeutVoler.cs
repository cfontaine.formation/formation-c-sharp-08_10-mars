﻿namespace _09_Polymorphisme
{
    interface IPeutVoler
    {
        void Decoller();
        void Atterir();
    }
}
