﻿using System;

namespace _09_Polymorphisme
{
    class Program
    {
        static void Main(string[] args)
        {
            //Animal a = new Animal(3, 4000);   // Impossible la classe est abstraite
            //a.EmettreUnSon();

            Chien ch1 = new Chien("Rollo", 5, 6000);
            ch1.EmettreUnSon();

            Animal a2 = new Chien("Laika", 6, 3000); // On peut créer une instance de Chien (classe fille) qui aura une référence, une référence Animal (classe mère)
                                                     // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux propriétés et aux méthodes propre au chien (Nom,...)
            a2.EmettreUnSon();                       // Comme la méthode est virtual dans Animal et est rédéfinie dans Chien, c'est la méthode de Chien qui sera appelée

            if (a2 is Chien)            // test si a2 est de "type" Chien
            {
                // Pour passer d'une super-classe à une sous-classe, il faut le faire explicitement avec un cast ou avec l'opérateur as
                // Chien ch2 =(Chien) a2;
                Chien ch2 = a2 as Chien;        // as equivalant à un cast pour un objet
                ch2.EmettreUnSon();             // avec la référence ch2 (de type Chien), on a bien accès à toutes les propriétées de la classe Chien
                Console.WriteLine(ch2.Nom);
            }

            if (a2 is Chien ch3)    // Avec l'opérateur is, on peut tester si a2 est de type Chien et faire la conversion en même temps 
            {
                Console.WriteLine(ch3.Nom);
            }

            Animal[] tab = new Animal[5];
            tab[0] = new Chien("Rollo", 5, 6000);
            tab[1] = new Chat(4, 9, 3000);
            tab[2] = new Chien("Laika", 6, 3000);
            tab[3] = new Chat(5, 6, 2300);
            tab[4] = new Chien("Idefix", 5, 1500);

            foreach (Animal an in tab)
            {
                an.EmettreUnSon();
            }

            // Object
            // ToString
            Console.WriteLine(a2);
            // Equals
            Chien cA = new Chien("Rollo", 5, 6000);
            Chien cB = new Chien("Rollo", 5, 6000);

            Console.WriteLine(cA == cB);        // si l'opérateur == n'est pas redéfinit, on compare l'égalité des références comme se sont 2 objets différents => false
            Console.WriteLine(cA.Equals(cB));

            Type type = cA.GetType();
            Console.WriteLine(type);
            Type type2 = typeof(Chat);
            Console.WriteLine(type2);

            // Interface
            IPeutMarcher pm1 = new Canard(4, 4000);     // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            pm1.Marcher();

            Console.ReadKey();
        }
    }
}
