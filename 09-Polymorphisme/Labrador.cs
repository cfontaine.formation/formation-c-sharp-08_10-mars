﻿namespace _09_Polymorphisme
{
    class Labrador : Chien
    {
        public Labrador(string nom, int age, double poid) : base(nom, age, poid)
        {
        }

        // Dans la classe Chien la Méthode EmettreUnSon est sealed, on ne peut plus la redéfinir
        // public override void EmettreUnSon()
        // { 
        //    Console.WriteLine($"{Nom} aboie");
        // }

    }
}
