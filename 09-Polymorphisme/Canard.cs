﻿using System;

namespace _09_Polymorphisme
{
    class Canard : Animal, IPeutMarcher, IPeutVoler    // On peut implémenter plusieurs d'interfaces
    {
        public Canard(int age, double poid) : base(age, poid)
        {
        }

        public override void EmettreUnSon()
        {
            Console.WriteLine("Coin coin");
        }

        public void Marcher()
        {
            Console.WriteLine("Le canard marche");
        }

        public void Courrir()
        {
            Console.WriteLine("Le canard court");
        }

        public void Decoller()
        {
            Console.WriteLine("Le canard décolle");
        }

        public void Atterir()
        {
            Console.WriteLine("Le canard atterrit");
        }
    }
}
