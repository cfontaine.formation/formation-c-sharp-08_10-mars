﻿using System;
using System.IO;

namespace _07_EntreeSortie
{
    class Program
    {
        static void Main(string[] args)
        {
            #region DriveInfo
            // DriveInfo fournit des informations sur les lecteurs d'une machine
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (var drv in drives)
            {
                Console.WriteLine(drv.Name);            // Nom du lecteur
                Console.WriteLine(drv.TotalFreeSpace);  // espace disponible sur le lecteur
                Console.WriteLine(drv.TotalSize);       // espace total du lecteur
                Console.WriteLine(drv.DriveFormat);     // système de fichiers du lecteur NTFS, FAT ..
                Console.WriteLine(drv.DriveType);       // Type de lecteur Fixed Removable
            }
            #endregion

            #region Directory
            //  Teste si le dossier existe
            if (!Directory.Exists(@"C:\Formations\TestIO\Csharp"))
            {
                Directory.CreateDirectory(@"C:\Formations\TestIO\Csharp");          // Création du répertoire
            }

            string[] paths = Directory.GetFiles(@"C:\Formations\TestIO\Csharp");    // Liste les fichiers du répertoire
            foreach (var f in paths)
            {
                Console.WriteLine(f);
            }

            paths = Directory.GetDirectories(@"C:\Formations\TestIO\Csharp");       // Liste les répertoires du répertoire
            foreach (var d in paths)
            {
                Console.WriteLine(d);
            }
            #endregion

            #region File
            if (!File.Exists(@"c:\Formations\TestIO\Csharp\a.txt"))
            {
                File.CreateText(@"c:\Formations\TestIO\Csharp\a.txt");  // Crée le fichier a.txt s'il n'existe pas
            }
            if (!File.Exists(@"c:\Formations\TestIO\Csharp\b.txt"))
            {
                File.CreateText(@"c:\Formations\TestIO\Csharp\b.txt");  // Crée le fichier b.txt s'il n'existe pas
            }
            if (!File.Exists(@"c:\Formations\TestIO\Csharp\c.txt"))     // si le fichier c.txt n'existe pas 
            {
                File.Move(@"c:\Formations\TestIO\Csharp\b.txt", @"c:\Formations\TestIO\Csharp\c.txt");  // On renomme le fichier b.txt en fichier c.txt
            }
            #endregion

            EcrireFichierText(@"C:\Formations\TestIO\Csharp\test.txt");
            LireFichierText(@"c:\Formations\TestIO\Csharp\test.txt");

            EcrireFichierBin(@"c:\Formations\TestIO\Csharp\test.bin");
            LireFichierBin(@"c:\Formations\TestIO\Csharp\test.bin");

            // Exercice Parcourir
            Parcourir(@"C:\Formations\TestIO");

            // Exercice Copie
            Copier(@"C:\Formations\TestIO\Csharp\logo.png", @"C:\Formations\TestIO\Csharp\copid_logo.png");
            Console.ReadKey();
        }

        public static void EcrireFichierText(string path)
        {
            StreamWriter sw = null;     // StreamWriter Ecrire un fichier texte
            try                         // Sans utiliser Using  
            {
                sw = new StreamWriter(path, true);      // append à true "compléte" le fichier s'il existe déjà, à false le fichier est écrasé
                for (int i = 0; i < 10; i++)
                {
                    sw.WriteLine("Hello world");
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                sw.Close();
                sw.Dispose();
            }
        }


        public static void LireFichierText(string path)
        {
            // Using => Équivalent d'un try / finally + Close()
            using (StreamReader sr = new StreamReader(path))    // StreamReader Lire un fichier texte
            {
                while (!sr.EndOfStream)  // Propriété EndOfStream est vrai si le fichier atteint la fin du fichier
                {
                    Console.WriteLine(sr.ReadLine());
                }
            }
        }

        public static void EcrireFichierBin(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Append))   // FileStream =>  permet de Lire/Ecrire un fichier binaire
            {
                for (byte b = 0; b < 100; b++)
                {
                    fs.WriteByte(b);    // Ecriture d'un octet dans le fichier
                }
            }
        }

        public static void LireFichierBin(string path)
        {
            byte[] tab = new byte[10];
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                int nb = 1;
                while (nb != 0)
                {
                    nb = fs.Read(tab, 0, 10);   // Lecture de 10 octets au maximum dans le fichier, ils sont placés dans le tableau tab à partir de l'indice 0
                    foreach (var v in tab)      // Read => retourne le nombre d'octets lue dans le fichier                     {
                        Console.WriteLine(v);
                }
            }
        }

        static void Parcourir(string path)
        {
            if (Directory.Exists(path))
            {
                string[] fileNames = Directory.GetFiles(path);
                foreach (string f in fileNames)
                {
                    Console.WriteLine(f);
                }
                string[] directoryNames = Directory.GetDirectories(path);
                foreach (string d in directoryNames)
                {
                    Console.WriteLine($"Répertoire {d}");
                    Console.WriteLine("_____________");
                    Parcourir(d);
                }
            }
            else if (!File.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

        }
        static void Copier(string pathSource, string pathTarget)
        {
            FileStream fsi = null;
            FileStream fso = null;
            try {
            fsi = new FileStream(pathSource, FileMode.Open);
            fso = new FileStream(pathTarget, FileMode.CreateNew);
            int b = 1;
            while (b!=-1)
            {
              b= fsi.ReadByte();
               if (b!=-1)
               {
                    fso.WriteByte((byte)b);
                }
             }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                if (fsi != null)
                {
                    fsi.Close();
                    fsi.Dispose();
                }
                if(fso!= null)
                {
                    fso.Close();
                    fso.Dispose();
                }
            }
        }
    }
}
