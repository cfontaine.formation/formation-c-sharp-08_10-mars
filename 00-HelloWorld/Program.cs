﻿using System;

namespace _00_HelloWorld
{

    /// <summary>
    /// Une Classe Hello World
    /// </summary>
    class Program
    {
        /// <summary>
        /// Point d'entrée du programme
        /// </summary>
        /// <param name="args">arguments de la ligne de commande</param>
        static void Main(string[] args) // Commentaire sur une ligne
        {
            /*
             * Commentaire
             * sur 
             * plusieurs lignes
             */
            Console.WriteLine("Hello World!!!");
            Console.ReadKey();  // Pour maintenir la console affichée
        }
    }
}
