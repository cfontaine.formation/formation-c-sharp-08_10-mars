﻿using System;
using System.Collections.Generic;
using System.IO;

namespace _07_Entree_sortie_Annuaire
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("1 - Ajouter Contact");
                Console.WriteLine("2 - Lister les contacts");
                Console.WriteLine("0 - Quitter");
                string option = Console.ReadLine();
                if (option == "0") break;

                switch (option)
                {
                    case "1":
                        AjouterContact("annuaire.csv");
                        break;
                    case "2":
                        List<string[]> contacts= ChargerContact("annuaire.csv");
                        ListerContact(contacts);
                        break;
                    default:
                        Console.WriteLine("L'option choisie n'existe pas");
                        break;
                }

                Console.ReadKey();
            }

        }

        public static void EnregistrerContact(string path, string nom, string prenom, string email)
        {
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format($"{nom};{prenom};{email}"));
            }
            Console.WriteLine("Le contact est bien ajouté.");
        }

        static void ListerContact(List<string[]> contacts)
        {
            if (contacts.Count > 0)
            {
                foreach (string[] contact in contacts)
                {
                    Console.WriteLine($"--------\n- Nom : {contact[0]}\n- Prenom : {contact[1]}\n- Email : {contact[2]}\n\n");
                }
            }
            else
            {
                Console.WriteLine("Il n'y a pas de contact enregistrer");
            }
        }

        static List<string[]> ChargerContact( string path)
        {
            List<string[]> contacts = new List<string[]>();

            using (StreamReader reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    string[] fields = reader.ReadLine().Split(';');
                    contacts.Add(fields);
                }
            }
            return contacts;
        }

        static void AjouterContact(string path)
        {
            Console.Clear();
            Console.WriteLine("Saisir le Nom : ");
            string nom = Console.ReadLine();
            Console.WriteLine("Saisir le Prenom : ");
            string prenom = Console.ReadLine();
            Console.WriteLine("Saisir l'Email : ");
            string email = Console.ReadLine();
            EnregistrerContact(path,nom, prenom, email);
        }
    }
}
