﻿using System;

namespace _11_SurchargeOperateur
{
    class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point(1, 2);
            Point b = new Point(3, 4);

            Console.WriteLine(-a);
            Console.WriteLine(a + b);
            Point res = a * 4;
            Console.WriteLine(res);
            res *= 10;
            Console.WriteLine(res);
            res += a;   // quand on surcharge un opérateur, l'opérateur composé associé est aussi automatiquement surchargé
            Console.WriteLine(res);

            // Exercice surcharge opérateur
            Fraction f1 = new Fraction(1, 4);
            Fraction f2 = new Fraction(1, 4);
            Fraction fr = f1 * f2;
            Console.WriteLine(fr);
            Console.WriteLine(f1 == f2);
            Console.WriteLine(f1 != f2);
            Console.WriteLine(f1 * 2);
            Console.WriteLine(f1 * 8);

            Console.ReadKey();
        }
    }
}
