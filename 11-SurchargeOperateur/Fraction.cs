﻿using System;

namespace _11_SurchargeOperateur
{
    // Exercice surcharge opéateur Classe Fraction
    // propriété
    // - numerateur
    // - denominateur si le denominate est égal à 0, on lance un exception
    // Constructeur
    //  qui prends les 2 paramètres avec pour valeur par défaut numerateur à 0.0 et denominateur à 1.0
    // Methode
    // -	une méthode calculer qui retourne le resultat de la fraction sous forme de double
    //-   une méthode privée qui permet de calculer le pgcd
    //    fonction pgcd(a, b)
    //        tant que a ≠ b
    //            si a > b alors
    //               a := a − b
    //            sinon
    //               b := b − a
    //        retourner a

    // Surcharge opérateur
    // - + entre 2 Fractions(utiliser la méthode pgcd pour simplifier la fraction)
    // - *  entre 2 Fractions(utiliser la méthode pgcd pour simplifier la fraction)
    // - *  entre une Fractions et un entier(utiliser la méthode pgcd pour simplifier la fraction)
    // - == et != les opérateurs retournent un booléen et prend en paramètre 2 Fractions
    // - tostring
    class Fraction
    {
        public int Numerateur { get; set; }
        public int Denominateur { get; set; }

        public Fraction(int numerateur, int denominateur)
        {
            Numerateur = numerateur;
            if (denominateur == 0)
            {
                throw new ArithmeticException("Denominateur égale à 0");
            }
            Denominateur = denominateur;
        }

        public double Calcul()
        {
            return ((double)Numerateur) / Denominateur;
        }

        public static Fraction operator +(Fraction f1, Fraction f2)
        {
            int n = f1.Numerateur * f2.Denominateur + f2.Numerateur * f1.Denominateur;
            int d = f1.Denominateur * f2.Denominateur;
            int p = Pgcd(n, d);
            return new Fraction(n / p, d / p);
        }

        public static Fraction operator *(Fraction f1, Fraction f2)
        {
            int n = f1.Numerateur * f2.Numerateur;
            int d = f1.Denominateur * f2.Denominateur;
            int p = Pgcd(n, d);
            return new Fraction(n / p, d / p);
        }

        public static Fraction operator *(Fraction f1, int scal)
        {
            int num = f1.Numerateur * scal;
            int p = Pgcd(num, f1.Denominateur);
            return new Fraction(num / p, f1.Denominateur / p);
        }

        public static bool operator ==(Fraction f1, Fraction f2)
        {
            return f1.Numerateur == f2.Numerateur && f1.Denominateur == f2.Denominateur;
        }

        public static bool operator !=(Fraction f1, Fraction f2)
        {
            return !(f1 == f2);
        }

        public override bool Equals(object obj)
        {
            return obj is Fraction fraction &&
                   Numerateur == fraction.Numerateur &&
                   Denominateur == fraction.Denominateur;
        }

        public override int GetHashCode()
        {
            int hashCode = 1421187769;
            hashCode = hashCode * -1521134295 + Numerateur.GetHashCode();
            hashCode = hashCode * -1521134295 + Denominateur.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            if (Denominateur == 1)
            {
                return string.Format($"{Numerateur}");
            }
            return string.Format($"{Numerateur}/{Denominateur}");
        }

        private static int Pgcd(int a, int b)
        {
            while (a != b)
            {
                if (a > b)
                {
                    a -= b;
                }
                else
                {
                    b -= a;
                }
            }
            return a;
        }

    }

}
