﻿using _10_EspaceNom.gui;
//Alias
using ConsoleSys = System.Console;

namespace _10_EspaceNom
{
    class Program
    {
        static void Main(string[] args)
        {
            Windows win = new Windows();
            _10_EspaceNom.dao.DaoUser dao = new _10_EspaceNom.dao.DaoUser();

            Console console = new Console();
            System.Console.WriteLine("Hello");
            ConsoleSys.WriteLine("Test");
            ConsoleSys.ReadKey();
        }
    }
}
