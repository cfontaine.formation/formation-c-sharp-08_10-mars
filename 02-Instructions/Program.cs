﻿using System;

namespace _02_Instructions
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Condition if
            int v = Convert.ToInt32(Console.ReadLine());
            if (v > 10)
            {
                Console.WriteLine("sup à 10");
            }
            else if (v == 10)
            {
                Console.WriteLine("egal à 10");
            }
            else
            {
                Console.WriteLine("inf  à 10");
            }

            // Exercice: Trie de 2 Valeurs
            // Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
            Console.WriteLine("Saisir 2 nombres");
            double a = Convert.ToDouble(Console.ReadLine());
            double b = Convert.ToDouble(Console.ReadLine());
            if (a > b)
            {
                Console.WriteLine($" {b} < {a}");
            }
            else
            {
                Console.WriteLine($" {a} < {b}");
            }

            // Exercice: Intervalle
            // Saisir un nombre et dire s'il fait parti de l'intervalle - 4(exclus) et 7(inclus)
            Console.WriteLine("Saisir un nombre entier");
            int valI = Convert.ToInt32(Console.ReadLine());
            if (valI > -4 && valI <= 7)
            {
                Console.WriteLine($"{valI} fait partie de l'intervalle -4 , 7 ");
            }
            #endregion

            #region Condition switch
            int jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Exercice: Calculatrice
            // Faire un programme calculatrice
            // Saisir dans la console
            // un double v1
            // une chaine de caractère opérateur qui a pour valeur valide: +-* /
            // un double v2

            // Afficher:
            // Le résultat de l’opération
            // Une message d’erreur si l’opérateur est incorrecte
            // Une message d’erreur si l’on fait une division par 0
            double v1 = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double v2 = Convert.ToDouble(Console.ReadLine());

            switch (op)
            {
                case "+":
                    Console.WriteLine($"{v1} + {v2} = {v1 + v2}");
                    break;
                case "-":
                    Console.WriteLine($"{v1} - {v2} = {v1 - v2}");
                    break;
                case "*":
                    Console.WriteLine($" {v1} * {v2} = {v1 * v2}");
                    break;
                case "/":
                    if (v2 == 0.0) // (v2>-0.000000000001 && v2< 0.000000000001)
                    {              // v2>-0.000001 && v2<0.000001 après un calcul, il ne vaut pas faire des tests
                                   // d'égalités mais tester un intervalle pour prendre en compte les erreurs de calculs
                                   // float , double sont des valeurs approchées. Ici, comme c'est une valeur saisie, on peut tester l'égalité
                        Console.WriteLine("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine($" {v1} / {v2} = { v1 / v2}");
                    }
                    break;
                default:
                    Console.WriteLine($"L'opérateur {op} est incorrecte");
                    break;
            }
            #endregion

            #region Condition: opérateur ternaire
            // Exercice: Valeur absolue
            // Saisir 1 chiffres et afficher la valeur absolue sous la forme |- 1.85| = 1.85
            double va = Convert.ToDouble(Console.ReadLine());
            double abs = va >= 0.0 ? va : -va;
            Console.WriteLine($"|{va}|= {abs}");
            #endregion

            #region Boucle
            // Boucle while
            double vb = Convert.ToDouble(Console.ReadLine());
            int j = 0;
            while (vb + j < 10.0)
            {
                j++;
            }

            // Boucle for
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("i= " + i);
            }

            // Exercice: Table de multiplication
            // Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9  
            //
            // 1 X 4 = 4
            // 2 X 4 = 8
            //  …
            // 9 x 4 = 36
            //
            // Si le nombre saisie est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur
            for (; ; ) // ou while(true) => boucle infinie
            {
                int val = Convert.ToInt32(Console.ReadLine());
                if (val < 1 || val > 9)
                {
                    break;
                }
                for (int i = 1; i < 10; i++)
                {
                    Console.WriteLine($"{i} x {val} = {val * i}");
                }
            }

            // Idem avec un do while
            //int val;
            //do
            //{
            //    val = Convert.ToInt32(Console.ReadLine());
            //    if (val >= 1 && val <= 9)
            //    {
            //        for (int i = 1; i < 10; i++)
            //        {
            //            Console.WriteLine($"{i} x {val} = {val * i}");
            //        }
            //    }
            //} while (val >= 1 && val <= 9);


            // Exercice: Quadrillage 
            // un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne  

            // ex: pour 2 3  
            // [ ][ ]  
            // [ ][ ]  
            // [ ][ ]  

            Console.Write("Entrer le nombre de colonne : ");
            int col = Convert.ToInt32(Console.ReadLine());
            Console.Write("Entrer le nombre de ligne : ");
            int row = Convert.ToInt32(Console.ReadLine());

            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    Console.Write("[ ]");
                }
                Console.Write("\n"); // ou Console.WriteLine("");
            }
            #endregion

            #region Instructions de branchement
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("i= " + i);
                if (i == 3)
                {
                    break;      // break => termine la boucle
                }
            }

            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    continue;   // continue => on passe à l'itération suivante
                }
                Console.WriteLine("i= " + i);
            }

            for (int r = 0; r < 10; r++)
            {
                for (int c = 0; c < 5; c++)
                {
                    Console.WriteLine($"c={c} r={r}");
                    if (r == 3)
                    {
                        goto EXIT_LOOP; // Utilisation de goto pour se branche sur le label EXIT_LOOP et quitter les 2 boucles imbriquées
                    }
                }
            }
        EXIT_LOOP:

            jours = 1;
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    goto case 6;        // Utilisation de goto pour transférer le contrôle à un case ou à l’étiquette par défaut d’une instruction switch
                case 2:
                    Console.WriteLine("Mardi");
                    goto default;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }
            #endregion

            Console.ReadKey();
        }
    }
}
