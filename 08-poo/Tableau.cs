﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_poo
{
    class Tableau
    {
        int[] elm;

        public int Length
        {
            get => elm.Length;
        }

        public Tableau(int size)
        {
            elm = new int[size];
        }

        public Tableau(int size, int initVal) : this(size)
        {
            for(int i = 0; i < elm.Length; i++)
            {
                elm[i] = initVal;
            }
        }

        // Indexeur
        public int this[int index]
        {
            get
            {
                if (index >= 0 && index < elm.Length)
                {
                    return elm[index];
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set
            {
                if (index >= 0 && index < elm.Length)
                {
                    elm[index] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        public void Afficher()
        {
            Console.Write("[");
            foreach (int v in elm){
                Console.Write($" {v}");
            }
            Console.WriteLine(" ]");
        }
    }
}
