﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_poo
{
    class VoiturePrioritaire : Voiture  // VoiturePrioritaire hérite de Voiture
    {
        // base => pour appeler le constructeur de la classe mère
        public VoiturePrioritaire(string Marque, string couleur, string plaqueIma,bool gyro) : base(Marque, couleur, plaqueIma)
        {
            Gyro = gyro;
        }

        public bool Gyro { get; private set; }

        public void AllumerGyro()
        {
            Gyro = true;
        }

        public void EteindreGyro()
        {
            Gyro = false;
            Console.WriteLine(Marque);
            Marque = "Honda";
        }

        // Redéfinition
        public new void Afficher ()
        {
            base.Afficher();
            Console.WriteLine($"Gyro={Gyro}");
        }
    }
}
