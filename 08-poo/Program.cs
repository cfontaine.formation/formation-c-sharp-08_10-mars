﻿using System;

namespace _08_poo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Voiture
            // Appel d'une méthode de classe
            Voiture.TestMethodeClasse();

            // Appel d'une variable de classe
            Console.WriteLine($"Compteur Voiture={Voiture.CompteurVoiture}");

            // Instantiation de la classe Voiture
            Voiture v1 = new Voiture("Ford", "Vert", "fr-1234-34");
            Console.WriteLine($"Compteur Voiture={Voiture.CompteurVoiture}");

            // Accès à une propriété en lecture (get)
            Console.WriteLine(v1.Vitesse);

            // Accès à une propriété en ecriture (set)
            v1.Vitesse = 20;

            // v1.Marque = "Dacia";  // On ne peut pas accéder en écriture à la propriété Marque => pas de set public
            Console.WriteLine(v1.Marque);

            // Appel d’une méthode d’instance
            v1.Accelerer(20);
            Console.WriteLine(v1.Vitesse);
            v1.Freiner(5);
            Console.WriteLine(v1.Vitesse);
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();
            Console.WriteLine(v1.EstArreter());

            Voiture v2 = new Voiture("Opel", "Gris", "fr-5946-AZ", 20, 200); //{ Vitesse = 20, CompteurKm = 200 }; // Initialiseur
            Console.WriteLine($"Compteur Voiture={Voiture.CompteurVoiture}");
            v2.Vitesse = 10;
            Console.WriteLine(v2.Vitesse);

            Voiture v3 = new Voiture("Honda", "Noir", "fr-5934-ER");
            v3.Accelerer(10);
            Console.WriteLine(Voiture.EgaliteVitesse(v2, v3));

            // Test de l'appel du destructeur
            Voiture v1A = v1;
            v1A.Accelerer(10);
            v1 = null;  // En affectant, les références v1 et v1A à null.Il n'y a plus de référence sur l'objet voiture
            v1A = null; // Il sera détruit lors de la prochaine execution du garbage collector

            // Forcer l'appel le garbage collector
            GC.Collect();
            Console.WriteLine($"Compteur Voiture={Voiture.CompteurVoiture}");
            #endregion

            #region indexeur
            Tableau t = new Tableau(5, 10);
            try
            {
                t[0] = 12;
                t[3] = -6;
                t.Afficher();
                Console.WriteLine(t[1]);

                t[10] = 34;
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            #endregion

            #region Classe Imbriquée
            // Si la classe imbriquée est public ou internal, on peut l'instancier en dehors du conteneur (à éviter)
            // Container.Element elm = new Container.Element(); 
            // elm.TestElementClasse();
            // elm.TestElementInstance(c);
            Conteneur c = new Conteneur();
            c.Test();
            #endregion

            #region  Classe partielle
            // Classe Partielle => classe définit sur plusieurs fichiers
            Form1 f1 = new Form1(123);
            f1.Afficher();
            #endregion

            #region Classe Statique
            //    Math m = new Math();  // Math est une classe static on peut pas l'instancier
            // Elle ne contient que des membres de classe

            // Méthode d'extension
            string str = "AZERTY";
            Console.WriteLine(str.Inverser());
            #endregion

            #region Agrégation
            // Agrégation = associer un objet avec un autre
            Adresse adrL = new Adresse("1, rue Esquermoise", "Lille", "59000");

            Personne per1 = new Personne("John", "Doe", adrL);
            Personne per2 = new Personne("Jane", "Doe", adrL);
            Personne per3 = new Personne("Alan", "Smithee", new Adresse("11 Rue Antoine Bourdelle", "Paris", "75015"));

            Voiture v4 = new Voiture("Jaune", "Fiat", "fr-1524-ER", per1);
            Voiture v5 = new Voiture("Rouge", "Toyota", "fr-5624-ER");
            v5.Proprietaire = per2;
            #endregion

            #region Exercice Compte Bancaire
            CompteBancaire cb = new CompteBancaire(100.0, per1);
            cb.Afficher();
            cb.Crediter(40.0);
            cb.Afficher();
            cb.Debiter(34.50);
            cb.Afficher();
            Console.WriteLine(cb.EstPositif());

            CompteBancaire cb2;
            cb2 = new CompteBancaire(per3);
            cb2.Afficher();
            #endregion
            Console.WriteLine($"Compteur Voiture={Voiture.CompteurVoiture}");

            #region Exercice Point
            Point p1 = new Point(1, 2);
            Point p2 = new Point(3, 4);
            p1.Afficher();
            p2.Afficher();
            p1.Deplacer(2, 0);
            p1.Afficher();
            Console.WriteLine(p2.Norme());
            Console.WriteLine(Point.Distance(p1, p2));
            #endregion

            # region Exercice Indexeur
            Point3d ptr3D = new Point3d(4, 5, 6);
            Console.WriteLine(ptr3D.X);
            Console.WriteLine(ptr3D[1]);
            Console.WriteLine(ptr3D.Y);
            Console.WriteLine(ptr3D[2]);
            Console.WriteLine(ptr3D.Z);
            Console.WriteLine(ptr3D[3]);
            #endregion

            #region Héritage
            VoiturePrioritaire vp1 = new VoiturePrioritaire("Renault", "orange", "FR-358-AA", false);
            vp1.Accelerer(40);
            vp1.AllumerGyro();
            Console.WriteLine(vp1.Gyro);
            vp1.Afficher();

            Voiture vp2 = new VoiturePrioritaire("Renault", "Bleu", "FR-388-ZE", false);
            vp2.Afficher();

            // Exercice Compte Epargne
            CompteEpargne ce = new CompteEpargne(0.75, per1);
            ce.Crediter(150.0);
            ce.Afficher();
            ce.CalculInterets();
            ce.Afficher();
            #endregion

            Console.ReadKey();
        }
    }
}
