﻿using System;

namespace _08_poo
{
    class Personne
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public Adresse Adresse { get; set; }

        public Personne(string prenom, string nom, Adresse adresse)
        {
            Prenom = prenom;
            Nom = nom;
            Adresse = adresse;
        }

        public void Afficher()
        {
            Console.WriteLine($"{Prenom} {Nom}");
            Adresse.Afficher();
        }
    }
}
