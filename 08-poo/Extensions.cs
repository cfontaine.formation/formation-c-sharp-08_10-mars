﻿namespace _08_poo
{
    // Méthodes d'extension pour ajouter des fonctionnalités à des classes ou des structures existantes
    // elle doit être définie dans une classe static
    static class Extensions
    {
        // Une méthode d'extension doit être static et le premier paramètre doit être : this typeEtendu nomParametre
        // Ici, on ajoute un méthode Inverser à la classe .Net string
        public static string Inverser(this string str)
        {
            string strInverse = "";
            for (int i = str.Length - 1; i >= 0; i--)
            {
                strInverse += str[i];
            }
            return strInverse;
        }
    }
}
