﻿using System;

namespace _08_poo
{
    //  sealed => pour empecher l'héritage de cette classe
    class Voiture
    {
        // Variables d'instances => Etat

        // On n'a plus besoin de déclarer les variables d'instances Marque,PlaqueIma, CompteurKm, elles seront générées automatiquement par les propriétées automatique
        // private string Marque;
        string _couleur = "Rouge";      // Variable d'instance utilisée par la propriété Couleur (C# 7.0) 
        // private string PlaqueIma;
        private int _vitesse;           // Variable d'instance utilisée par la propriété Vitesse (propriété avec une condition)
        // private int CompteurKm = 100;

        // Variable de classe
        // private static int CompteurVoiture;    // Remplacer par une propriétée static

        // Propriété automatique => la variable d'instance est générée par le compilateur
        public string Marque { get; protected set; }  // On peut associer un modificateur d'accès à get et à set. Il doit être plus restritif que le modificateur de la propriété 
        public string PlaqueIma { get; set; }
        public int CompteurKm { get; private set; } = 100;  // On peut donner un valeur par défaut à une propriétée (littéral, expression ou une fonction)
        public Personne Proprietaire { get; set; }
        public static int CompteurVoiture { get; private set; }

        // Propriété
        public int Vitesse
        {
            get
            {
                return _vitesse;
            }
            set
            {
                if (value > 0)
                {
                    _vitesse = value;

                }
            }
        }

        // Propriété C# 7.0
        public string Couleur
        {
            get => _couleur;
            set => _couleur = value;
        }

        // Accesseur (Java/C++) en C# on utilisera les propriétés
        // Getter (accès en lecture)
        //public int GetVitesse()
        //{
        //    return Vitesse;
        //}
        // Setter (accès en écriture)
        //public void SetVitesse(int vitesse)
        //{
        //    if (vitesse > 0) {
        //        Vitesse=vitesse;
        //    }
        //}

        // Constructeurs => On peut surcharger le constructeur
        // Constructeur par défaut
        public Voiture()
        {
            Console.WriteLine("Constructeur par défaut");
            CompteurVoiture++;
        }

        // this() => Chainnage de constructeur : appel du constructeur par défaut
        public Voiture(string Marque, string couleur, string plaqueIma) : this()
        {
            Console.WriteLine("Constructeur 3 paramètres");
            this.Marque = Marque;
            _couleur = couleur;
            PlaqueIma = plaqueIma;
        }

        // Chainnage de constructeur : appel du constructeur  Voiture(string marque, string couleur, string plaqueIma)
        public Voiture(string marque, string couleur, string plaqueIma, int vitesse, int compteurKm) : this(marque, couleur, plaqueIma)
        {
            Vitesse = vitesse;
            CompteurKm = compteurKm;
        }

        public Voiture(string couleur, string marque, string plaqueIma, Personne proprietaire) : this(couleur, marque, plaqueIma)
        {
            Proprietaire = proprietaire;
        }

        // Exemple de constructeur static: appelé avant la création de la première instance ou le référencement d’un membre statique
        static Voiture()
        {
            Console.WriteLine("Constructeur static");
        }

        // Destructeur => libérer des ressources 
        ~Voiture()
        {
            CompteurVoiture--;
            Console.WriteLine("appel destructeur");
        }

        // Méthodes d'instances => comportement
        public void Accelerer(int vAcc)
        {
            if (vAcc > 0)
            {
                Vitesse += vAcc;
            }
        }

        public void Freiner(int vFrn)
        {
            if (vFrn > 0)
            {
                Vitesse -= vFrn;
            }
            if (Vitesse < 0)
            {
                Vitesse = 0;
            }
        }

        public void Arreter()
        {
            Vitesse = 0;
        }

        public bool EstArreter()
        {
            return Vitesse == 0;
        }

        public void Afficher()
        {
            Console.WriteLine($"{Marque} {Couleur} {PlaqueIma} {Vitesse} {CompteurKm}");
        }

        // Méthode de classe
        public static void TestMethodeClasse()
        {
            Console.WriteLine("Methode de classe");
            // Vitesse = 0;  // Dans une méthode de classe on n'a pas accès à une variable d'instance
            Console.WriteLine($"Méthode de classe, nb voiture:{CompteurVoiture}"); // On peut accéder dans une méthode de classe à une variable de classe
        }

        public static bool EgaliteVitesse(Voiture va, Voiture vb)
        {
            return va.Vitesse == vb.Vitesse;    // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        }
    }
}
