﻿using System;

namespace _08_poo
{
    // Classe imbriquée
    class Conteneur
    {
        private static string varClasse = " Variable de classe";

        private string varInstance = " Variable d'instance";

        public void Test()
        {
            Element e = new Element();
            e.TestElementClasse();
            e.TestElementInstance(this);
        }

        class Element // par défaut private
        {
            public void TestElementClasse()
            {
                Console.WriteLine(varClasse);   // On a accès au variable de classe de extérieur
            }
            public void TestElementInstance(Conteneur c)
            {
                Console.WriteLine(c.varInstance);
            }

        }

    }
}
