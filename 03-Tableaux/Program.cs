﻿using System;

namespace _03_tableaux
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Tableau à une dimension
            // Déclaration d'un tableau
            char[] tabChr;          // ou char[] tabChr = new char[5] 
            tabChr = new char[5];

            // Valeur d'initialisation par défaut des éléments du tableau
            // - entier -> 0
            // - nombre à virgule flottante -> 0.0
            // - bool -> false
            // - char -> '\u0000'
            // - type référence -> null

            Console.WriteLine(tabChr[0]);    // accès au premier élément du tableau
            tabChr[0] = 'a';
            tabChr[1] = 'z';
            tabChr[2] = 'e';
            tabChr[3] = 'r';
            tabChr[4] = 't';
            Console.WriteLine(tabChr[0]);

            Console.WriteLine(tabChr.Length);    // Length=>Nombre d'élément du tableau

            // Déclarer un tableau en l'initialisant
            int[] tab1 = { 10, 4, 7, 8, 9 };
            Console.WriteLine(tab1[2]); //7

            // tabChr[10] = '1'; // si l'on essaye d'accèder à un élément en dehors du tableau => exception

            // Parcourir un tableau avec un for
            for (int i = 0; i < tabChr.Length; i++)
            {
                Console.WriteLine("tab[{0}]={1}", i, tabChr[i]);
            }

            // Parcourir complétement un tableau
            foreach (var chr in tabChr)
            {
                Console.WriteLine(chr);
            }
            #endregion

            #region Exercice: Tableau
            // 1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7,4,8,0,-3
            // 2 - Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau

            //int[] tab = { -7, 4, 8, 0, -3 };                  // 1

            Console.WriteLine("Saisir la taille du tableau");   //2
            int size = Convert.ToInt32(Console.ReadLine());
            int[] tab = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"tab[{i}]=");
                tab[i] = Convert.ToInt32(Console.ReadLine());
            }

            int maximum = tab[0]; //int.MinValue;
            double somme = 0.0;
            foreach (var t in tab)
            {
                if (t > maximum)
                {
                    maximum = t;
                }
                somme += t;
            }
            Console.WriteLine($"maximum={maximum} moyenne={somme / tab.Length}");
            #endregion

            #region  Tableau Multidimmension
            int[,] tab2d = new int[3, 2];   // Déclaration d'un tableau à 2 dimensions
            tab2d[2, 1] = 5;                // Accès à un élémént d'un tableau à 2 dimensions

            Console.WriteLine(tab2d.Length); // Nombre d'élément du tableau    => 6
            Console.WriteLine(tab2d.Rank);   // Nombre de dimension du tableau => 2
            Console.WriteLine($"Nombre de ligne={tab2d.GetLength(0)},Nombre de colonne={tab2d.GetLength(1)}");

            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var vi in tab2d)
            {
                Console.WriteLine(vi);
            }

            // Parcourir un tableau à 2 dimension avec un for
            for (int i = 0; i < tab2d.GetLength(0); i++)
            {
                for (int j = 0; j < tab2d.GetLength(1); j++)
                {
                    Console.Write($"{tab2d[i, j]}\t");
                    ;
                }
                Console.WriteLine("");
            }

            // Déclarer un tableau à 2 dimension en l'initialisant
            string[,] tab2dStr = { { "azer", "tyu", "tiop" }, { "sdggh", "gjjh", "vcvbd" } };

            // Tableau 3D
            // Déclaration d'un tableau à 3 dimensions
            int[,,] tab3d = new int[4, 3, 4];   // Accès à un élémént d'un tableau à 3 dimensions
            tab3d[1, 1, 2] = 356;
            Console.WriteLine(tab3d.Length);    // Nombre d'élément du tableau    => 48
            Console.WriteLine(tab3d.Rank);      // Nombre de dimension du tableau => 3
            #endregion

            #region Tableau de tableau
            // Déclaration d'un tableau de tableau
            char[][] tabNc = new char[3][];
            tabNc[0] = new char[3];
            tabNc[1] = new char[4];
            tabNc[2] = new char[2];

            tabNc[0][2] = 'z';  // accès à un élément

            Console.WriteLine("Nombre de ligne={tabNc.Length}");   // Nombre de Ligne

            for (int i = 0; i < tabNc.GetLength(0); i++)
            {
                Console.WriteLine($"ligne={i} nb colonne= {tabNc[i].Length}");  // Nombre de colonne pour chaque ligne
            }

            //  Parcourir complétement un tableau de tableau
            foreach (char[] tl in tabNc)
            {
                foreach (char chr in tl)
                {
                    Console.Write($"|{chr}|");
                }
                Console.Write("\n");
            }
            #endregion
            Console.ReadKey();
        }
    }
}
