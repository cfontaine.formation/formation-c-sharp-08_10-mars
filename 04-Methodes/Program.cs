﻿using System;

namespace _04_Methodes
{
    class Program
    {
        static void Main(string[] args)
        {
            // Appel de methode
            double res = Multiplication(2.5, 6.36);
            Console.WriteLine(res);

            // Appel de methode (sans retour)
            Afficher(42);

            // Exercice maximum
            Console.WriteLine(Maximum(1.4, 10.6));
            Console.WriteLine(Maximum(10.6, 1.4));
            double r = Maximum(10.0, 10.0);
            Console.WriteLine(r);

            // Exercice parité          
            Console.WriteLine(Even(3));
            Console.WriteLine(Even(2));

            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int v = 10;
            TestParamValeur(v);
            Console.WriteLine("Valeur=" + v);

            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            TestParamReference(ref v);
            Console.WriteLine("Ref=" + v);

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée
            int a;
            double b;
            TestParamSortie(3, out a, out b);
            Console.WriteLine($"a= {a} b={b}");

            // Déclarer la variable de retour dans les arguments pendant l'appel de la méthode
            TestParamSortie(3, out int a1, out double b1);
            Console.WriteLine($"a= {a1} b={b1}");

            // On peut ignorer un paramètre out en le nommant _
            TestParamSortie(3, out _, out double b2);
            Console.WriteLine($"b={b2}");

            // Paramètre optionnel
            TestParamOptionnel(10, "Bonjour", true);
            TestParamOptionnel(30);
            TestParamOptionnel(700, "World");

            // Paramètres nommés
            TestParamOptionnel(b: true, i: 123, str: "azerty");
            TestParamOptionnel(345, b: true, str: "azerty");
            TestParamOptionnel(789, b: true);

            // Nombre de paramètres variable
            Console.WriteLine(Moyenne());
            Console.WriteLine(Moyenne(3.0, 4.0, 1.2));

            // Surcharge 
            Console.WriteLine(Somme(1, 2));
            Console.WriteLine(Somme(3.7, 56.7));
            Console.WriteLine(Somme(2, 6.7));
            Console.WriteLine(Somme("hello", "world"));
            //Console.WriteLine(Somme( 6M,2L));
            Console.WriteLine(Somme(6L, 2L));

            // Méthode récursive
            int resR = Factorial(3);
            Console.WriteLine(resR);

            // Affichage des paramètres passés au programme (main)
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> options de démarrage -> Arguments de la ligne de commande 
            foreach (var ar in args)
            {
                Console.WriteLine(ar);
            }

            // Exercice Tableau
            Menu();

            // Appel de AfficherTab 
            int[] tab = { 10, 6, 5 };
            AfficherTab(tab);
            Console.ReadKey();
        }

        static double Multiplication(double v1, double v2)
        {
            return v1 * v2; // L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur (expression à droite)
        }
        static void Afficher(int i) // void => pas de valeur retournée
        {
            Console.WriteLine(i);
            // avec void => return; ou return peut être omis 
        }

        #region Passage de paramètre
        // Passage par valeur
        static void TestParamValeur(int a)
        {
            Console.WriteLine(a);
            a = 34;      // La modification de la valeur du paramètre a n'a pas d'influence en dehors de la méthode
            Console.WriteLine(a);
        }

        // Passage par référence => ref
        static void TestParamReference(ref int a)
        {
            Console.WriteLine(a);
            a++;
            Console.WriteLine(a);
        }

        // Passage de paramètre en sortie => out
        static void TestParamSortie(int entre, out int ra, out double rb)
        {
            ra = 20 * entre;  // La méthode doit obligatoirement affecter une valeur aux paramètres out
            Console.WriteLine(ra);
            rb = 20.6;
        }

        // Pour les arguments passés par valeur, on peut leurs donner des valeurs par défaut
        static void TestParamOptionnel(int i, string str = "Hello", bool b = false)
        {
            Console.WriteLine($"i={i} str={str} b={b}");
        }
        #endregion

        #region Exercice Maximum
        // Ecrire une méthode Maximum qui prends en paramètre 2 nombres et elle retourne le maximum
        static double Maximum(double a, double b)
        {
            return a > b ? a : b;

            // ou
            //if(a > b)
            //{
            //    return a;
            //}else
            //{
            //    return b;
            //}
        }
        #endregion

        #region Exercice Parité
        // Écrire une méthode even qui prend un entier en paramètre
        // Elle retourne vrai, si il est paire
        static bool Even(int val)
        {
            return val % 2 == 0;

            // ou
            //if (val % 2 == 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}

            // ou
            // return val % 2 == 0 ? true : false;
        }
        #endregion

        // Nombre d'arguments variable => params
        static double Moyenne(params double[] valeur)
        {
            if (valeur.Length == 0)
            {
                return 0.0;
            }
            else
            {
                double somme = 0.0;
                foreach (var v in valeur)
                {
                    somme += v;
                }
                return somme / valeur.Length;
            }
        }

        #region Surcharge_de_méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature
        static int Somme(int a, int b)
        {
            Console.WriteLine("2 entiers");
            return a + b;
        }

        static double Somme(double va, double vb)
        {
            Console.WriteLine("2 double");
            return va + vb;
        }

        static double Somme(int a, double b)
        {
            Console.WriteLine("un int et un double");
            return a + b;
        }

        static string Somme(string s1, string s2)
        {
            Console.WriteLine("2 chaines");
            return s1 + s2;
        }
        #endregion
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1)// condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }

        #region Exercice Tableau
        // Écrire un méthode qui affiche un tableau d’entier
        // Écrire une méthode qui permet de saisir :
        //  - La taille du tableau
        //  - Les éléments du tableau
        // Écrire une méthode qui calcule :
        // - le minimum d’un tableau d’entier
        // - le maximum
        // - la moyenne
        // Faire un menu qui permet de lancer ces méthodes

        static void AfficherTab(int[] tab)
        {
            Console.Write("[ ");
            foreach (int v in tab)
            {
                Console.Write($"{v} ");
            }
            Console.WriteLine("]");
        }

        static int[] SaisirTab()
        {
            Console.WriteLine("Saisir la taille du tableau");
            int size = Convert.ToInt32(Console.ReadLine());

            int[] tab = new int[size];
            for (int i = 0; i < tab.Length; i++)
            {
                Console.Write($"tab[{i}]=");
                tab[i] = Convert.ToInt32(Console.ReadLine());
            }
            return tab;
        }

        static void CalculTab(int[] tab, out int min, out int max, out double moy)
        {
            min = int.MaxValue;
            max = int.MinValue;
            double somme = 0.0;
            foreach (int t in tab)
            {
                somme += t;
                if (t < min)
                {
                    min = t;
                }
                else if (t > max)
                {
                    max = t;
                }
            }
            moy = somme / tab.Length;
        }

        static void AfficherMenu()
        {
            Console.WriteLine("1 - Afficher le tableau");
            Console.WriteLine("2 - Saisir le tableau");
            Console.WriteLine("3 - Afficher le minimum, le maximum et la moyenne");
            Console.WriteLine("0 - Quitter");
        }

        static void Menu()
        {
            AfficherMenu();
            int choix;
            int[] tab = null;
            do
            {
                Console.WriteLine("Choix=");
                choix = Convert.ToInt32(Console.ReadLine());
                if (tab == null && (choix == 1 || choix == 3))
                {
                    Console.WriteLine("Le tableau n'est pas saisie");
                }
                else
                {
                    switch (choix)
                    {
                        case 1:
                            AfficherTab(tab);
                            break;
                        case 2:
                            tab = SaisirTab();
                            break;
                        case 3:
                            CalculTab(tab, out int minimum, out int maximum, out double moyenne);
                            Console.WriteLine($"Minimum={minimum} , Maximum={maximum}, Moyenne={moyenne}");
                            break;
                        case 0:
                            Console.WriteLine("Au revoir");
                            break;
                        default:
                            Console.WriteLine($"Le choix {choix}n'existe pas\n");
                            AfficherMenu();
                            break;
                    }
                }
            }
            while (choix != 0);
        }
        #endregion
    }
}
